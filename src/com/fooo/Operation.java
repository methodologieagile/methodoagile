package com.fooo;

public class Operation {

	private String Resultat="";
    private String nb1="";
    private String nb2="";
    private String operateur;
    private boolean premier = true; 
    private String memoire="0";
    private boolean virgule1=false;
    private boolean virgule2=false;
	
	public Operation() {		
		MR();
        memoire = "0";
	}
		
	public void MR () {
		Resultat = "";
		nb1="";
        nb2="";
        operateur="";
        premier = true;
        virgule1=false;
        virgule2=false;
    }
	
	public void calcResultat() {
        Float t = (float) 0 ;
        if ("+".equals(operateur) )
        {
            t =  Additioner(Float.parseFloat(nb1), Float.parseFloat(nb2));
        }
        else if ("-".equals(operateur) )
        {
            t = Soustraire(Float.parseFloat(nb1), Float.parseFloat(nb2));
        }
        else if ("*".equals(operateur) )
        {
            t = Multiplier(Float.parseFloat(nb1), Float.parseFloat(nb2));
        }
        else if ("/".equals(operateur) )
        {
        	if(nb2 == "0")
        	{
        		t = Diviser(Float.parseFloat(nb1), Float.parseFloat(nb2));
        	}
        	else
        	{
        		t = (float) 0;
        	}
            
        }
        else
        {
        	t = Float.parseFloat(nb1);
        }
        
        Resultat = String.valueOf(t) ;
                
        nb1 = String.valueOf(t); 
        nb2 = "";
        premier = true;
        operateur = "";
        virgule1=true;
        virgule2=false;
    }
    
    
    public void ajouterAuNombre (String chiffre) {
    	
    	boolean calculNonValide = false;
    	
    	if(chiffre == "."){
    		if ( premier == true && virgule1 == false)
            {
    			virgule1 = true;			
            }
    		else if ( premier == false && virgule2 == false)
    		{
    			virgule2 = true;
    		}
    		else
    		{
    			calculNonValide = true;
    		}
    	}
    	
    	if (calculNonValide == false)
    	{
    		Resultat= Resultat+ chiffre ; 

            // premier chiffre      
            if ( premier == true)
            {
                nb1 = nb1 + chiffre;
            }
            else
            {
                nb2 = nb2 + chiffre;
            } 
    	}           
    }
    
  /**
 * @param operateur : opérateur de l'opération
 */
public void setOperateur(String operateur) {
    if (premier == true){
      Resultat = Resultat + operateur;
      premier = false;
      this.operateur = operateur; 
    }
  }
    
  public void SupprimerCaractere () {
    String nouveau_num = "" ;
    String dernierElement = "";    
        
    if (Resultat.charAt(Resultat.length()-1) == '*' || Resultat.charAt(Resultat.length()-1) == '/' || Resultat.charAt(Resultat.length()-1) == '-' || Resultat.charAt(Resultat.length()-1) == '+' )
    {
      premier = true ;
      operateur = "";
    }
    else if (premier == true)
    {            	
      dernierElement = dernierElement + nb1.charAt(nb1.length()-1);        	
        	if(dernierElement == ".")
        	{
        		virgule1 = false;
        	}
        	
            for (int i = 0; i < (nb1.length()-1) ; i++) {
                nouveau_num = nouveau_num+ nb1.charAt(i);
            }
            nb1 = nouveau_num;
        }
        else
        {
        	
        	dernierElement = dernierElement + nb2.charAt(nb2.length()-1);
        	
        	if(dernierElement == ".")
        	{
        		virgule2 = false;
        	}
        	
            for (int i = 0; i < (nb2.length()-1) ; i++) {
                nouveau_num = nouveau_num+ nb2.charAt(i);
            }
            nb2 = nouveau_num; 
        }  
        
        nouveau_num = "" ;
        for (int i = 0; i < (Resultat.length()-1) ; i++) {
                nouveau_num = nouveau_num+ Resultat.charAt(i);
        }
        
        Resultat = nouveau_num;
    }
	
	public String getResultat() {      
        return Resultat;
    }

    public String getNb1() {
        return nb1;
    }

    public void setNb1(String nb1) {
        this.nb1 = nb1;
    }

    public String getNb2() {
        return nb2;
    }

    public void setNb2(String nb2) {
        this.nb2 = nb2;
    }

    public String getOperateur() { 
        return operateur;
    }

    public boolean isPremier() {
        return premier;
    }

    public void setPremier(boolean premier) {
        this.premier = premier;
    }

    public void setResultat(String Resultat) {
        this.Resultat = Resultat;
    }
    
    public String getMemoire() {
        return memoire;
    }

    public void setMemoire(String memoire) {
        this.memoire = memoire;
    }
    
    public void AdditionerMemoire(){
    	
    	Float t;
    	
    	if ( premier == true)
        {
    		t = Float.parseFloat(memoire) + Float.parseFloat(nb1);
        }
        else
        {
        	t = Float.parseFloat(memoire) + Float.parseFloat(nb2);
        } 
    	
    	memoire = String.valueOf(t) ; 
    }
    
    public void SoustraireMemoire(){
    	
    	Float t;
    	
    	if ( premier == true)
        {
    		t = Float.parseFloat(memoire) - Float.parseFloat(nb1);
        }
        else
        {
        	t = Float.parseFloat(memoire) - Float.parseFloat(nb2);
        } 
    	
    	memoire = String.valueOf(t);
    	  	
    }
    
    public void initialiserMemoire(){
        memoire = "0";
    }
    
    public Float Additioner(Float num1, Float num2){
        return num1 + num2;
    }
    
    public Float Soustraire(Float num1, Float num2){
        return num1 - num2;
    }
    
    public Float Multiplier(Float num1, Float num2){
        return num1 * num2;
    }
    
    public Float Diviser(Float num1, Float num2){
        return num1 / num2;
    }
}
