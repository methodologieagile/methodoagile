package com.fooo;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Label;

public class Calculator extends SelectorComposer<Component> {
  String resString = "";
  Operation ope = new Operation();
  @Wire
  private Label valueOfLabel;

     // Number
  @Listen("onClick = #zeroButton")
  public void zeroButton(){
    addNumber("0");
  }
  
  @Listen("onClick = #oneButton")
  public void oneButton(){
    addNumber("1");
  }
  
  @Listen("onClick = #twoButton")
  public void twoButton(){
    addNumber("2");
  }
  @Listen("onClick = #threeButton")
  public void threeButton(){
    addNumber("3");
  }
  @Listen("onClick = #fourButton")
  public void fourButton(){
    addNumber("4");
  }
  @Listen("onClick = #fiveButton")
  public void fiveButton(){
    addNumber("5");
  }
  @Listen("onClick = #sixButton")
  public void sixButton(){
    addNumber("6");
  }
  @Listen("onClick = #sevenButton")
  public void sevenButton(){
    addNumber("7");
  }
  @Listen("onClick = #eightButton")
  public void eightButton(){
    addNumber("8");
  }
  @Listen("onClick = #nineButton")
  public void nineButton(){
    addNumber("9");
  }
  
  @Listen("onClick = #addButton")
  public void addButton(){
    addOperateur("+");
  }	
  
  @Listen("onClick = #minusButton")
  public void minusButton(){
    addOperateur("-");
  }

  @Listen("onClick = #timeButton")
  public void timeButton(){
    addOperateur("*");
  }
  
  @Listen("onClick = #devidedButton")
  public void devidedButton(){
    addOperateur("/");
  }
	 
  @Listen("onClick = #initButton")
  public void initButton(){
		 ope.MR();
		 resString = String.valueOf("") ;
			valueOfLabel.setValue(resString);
	    }
	 
  @Listen("onClick = #deleteButton")
  public void deleteButton(){
	  ope.SupprimerCaractere();
	  resString = String.valueOf(ope.getResultat()) ;
			valueOfLabel.setValue(resString);
	 	}
	 
  @Listen("onClick = #equalButton")
  public void register(){
    ope.calcResultat();
    resString = String.valueOf(ope.getResultat()) ;
    valueOfLabel.setValue(resString);
  }

  public void addNumber(String number){
    ope.ajouterAuNombre(number);
    resString = String.valueOf(ope.getResultat()) ;
    valueOfLabel.setValue(resString);
  }
  
  public void addOperateur(String operator){
		 ope.setOperateur(operator);
		 resString = String.valueOf(ope.getResultat()) ;
		 valueOfLabel.setValue(resString);
	 }
	 
  @Listen("onClick = #mPlusButton")
  public void mPlusButton(){
		 ope.AdditionerMemoire();
	    }
	 
  @Listen("onClick = #mMinusButton")
  public void mMinusButton(){
		 ope.SoustraireMemoire();
	    }
	 
  @Listen("onClick = #MRButton")
  public void MRButton(){	 
		 resString = String.valueOf(ope.getMemoire()) ;
		 valueOfLabel.setValue(resString);
	    }
	 
  @Listen("onClick = #MCButton")
  public void MCButton(){
		 ope.initialiserMemoire();
	    }
	 
  @Listen("onClick = #virguleButton")
  public void virguleButton(){
		 addNumber(".");
	    }
	 
	 
}
